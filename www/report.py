#!/usr/bin/python3
# Turn on debug mode.
import sys
import cgitb
import cgi
#import ldap

cgitb.enable()
# Print necessary headers.
print("Content-Type: text/html; charset=UTF-8\n\n")
# Connect to the database.
import pymysql
conn = pymysql.connect(
db='ats',
user='user',
passwd='password',
host='localhost',
charset='utf8')
c = conn.cursor()

# Print the contents of the database.
c.execute("SELECT * FROM ext_co_line")

print("<html><head><meta charset=\"utf-8\"><title>Телефоны</title></head><body>")
print("<table width=100%><tr valign=center><td><img src=img/logo-1.png></td><td width=100%><h2 align=center>ТЕЛЕФОНЫ</h2></td></table>")
print("<p><hr>[<a href=index.py>Справочник</a>] [Отчёты]<br><hr></p><p>")
print("<h3>Отчёты по звонкам</h3>")
print("<table><tr valign=top><td bgcolor=#e3e3e3 align=right>")
print("<form action=report.py><input type=hidden name=qwery_type value=internal>Внут.номер <input type=text name=internal value=0><br>")
print("За период с <input type=text name=date_begin value=0><br>по <input type=text name=date_end value=0>")
print("<p><input type=submit></p></form></td><td>")

param = cgi.FieldStorage()
#Передали заголовки ранее

if "qwery_type" not in param:
    print("Введите параметры отчёта")
    qwery_type = "last_month"
else:
    qwery_type = param["qwery_type"].value
    int_number = param["internal"].value
    dt_begin = param["date_begin"].value
    dt_end = param["date_end"].value

# строим запрос согласно параметров
if qwery_type == "internal":
    # если все параметры по нулям то вываливаем весь список
    if int_number == "0" and dt_begin == "0" and dt_end == "0":
        print("<h4 align=center>Список звонков</h4>")
        qwr = "SELECT * FROM cdr LIMIT 1000"
    elif int_number != "0" and dt_begin == "0" and dt_end == "0":
	# если задан номер но не задан период то вываливаем все звонки с этого номера
        print("<h4 align=center>Список звонков с номера " + int_number + "</h4>")
        qwr = "SELECT * FROM cdr WHERE int_number=" + int_number + " LIMIT 1000"
    elif int_number != "0" and dt_begin != "0" and dt_end != "0":
	# если задан номер и период то и так понятно
        print("<h4 align=center>Список звонков с номера " + int_number + "</h4>")
        print("За период с %s по %s" % (dt_begin, dt_end))
        qwr = "SELECT * FROM cdr WHERE call_date BETWEEN CAST(\'" + dt_begin + "\' AS DATE) AND CAST(\'" + dt_end + "\' AS DATE) AND int_number=" + int_number + " LIMIT 1000"
    elif int_number == "0" and dt_begin != "0" and dt_end != "0":
	# если номер незадан а задан период то вываливаем звонки со всех номеров за согласно введенных дат
        print("<h4 align=center>Список звонков</h4>")
        print("За период с %s по %s" % (dt_begin, dt_end))
        qwr = "SELECT * FROM cdr WHERE call_date BETWEEN CAST(\'" + dt_begin + "\' AS DATE) AND CAST(\'" + dt_end + "\' AS DATE) LIMIT 1000"

    c.execute(qwr)
    print("<table><tr bgcolor=#ffb7b7 align=center><td><b>№ п/п</b></td><td><b>Дата</b></td><td><b>Время</b></td><td><b>Внут.номер</b></td><td><b>Внеш.линия</b></td><td><b>Вызываемый номер</b></td><td><b>Ring</b></td><td><b>Длительность</b></td><td><b>АСС</b></td><td><b>Код звонка</b></td><td><b>Направление</b></td><tr>")
    for row in c.fetchall():
        print("<td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>" % (row[0], row[1], row[2], row[3], row[4], row[5], row[5], row[7], row[9], row[9], row[10]))
    print("</table>")
elif qwery_type == 'ldap':


print("</td></tr></table>")
print("</body></html>")