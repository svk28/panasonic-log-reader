#!/usr/bin/tclsh
######################################################
#        Panasonic TDA200 call data collector
#        Distributed under GNU Public License
# Author: Sergey Kalinin banzaj28@gmail.com
# Home page: 
######################################################
package require mysqltcl

# параметры соединения с СУБД

#set db(host) "host"
#set db(user) "user"
#set db(pass) "password"
#set db(dbname) "ats_test"
#set out_dir "~/tmp/ats"

proc InsertData {} {
    global arrVariables db
    puts [array size arrVariables]
    set qwery "INSERT INTO `cdr` ("
    foreach key [array names arrVariables] {
        set qwery "$qwery `$key`, "
    }
    set qwery "[string trimright  $qwery ", "]\) VALUES\("
    foreach key [array names arrVariables] {
        set qwery "$qwery \"[string trim $arrVariables($key)]\","
    }
    set qwery "[string trimright $qwery ", "]\);"
    puts $qwery
    set conn [mysql::connect -host $db(host) -db $db(dbname) -user $db(user) -password $db(pass) -encoding utf-8]
    mysql::exec $conn $qwery
    mysql::commit $conn
    mysql::close $conn
}

proc ParceString {line} {
    global out_dir arrVariables
    # Получает на вход строку и раскидывает её в нужном виде и пишет в файл
    if {[string range $line 0 2] == "---" || $line == "" || [string range $line 3 6] == "Date"} {
        #puts $line
        return
    }
    # Создаём текстовые файлы на всякий случай, для дублирования информации
    set fName [clock format [clock scan "now" -base [clock seconds]] -format %m_%Y]
    set out_log_name [file join $out_dir $fName]
    set out_log [open $out_log_name "a+"]
    puts $out_log "$line"
    close $out_log
    # Разбор строки
    # Преобразуем дату к виду "ДД/ММ/ГГГГ"
    set arrVariables(call_date) "20[string range $line 6 7]\/[string range $line 3 4]\/[string range $line 0 1]"
    set arrVariables(call_time) [string range $line 9 13]
    set arrVariables(int_number) [string range $line 19 21]
    set arrVariables(ext_co_line) [string range $line 23 24]
    set arrVariables(dial_number) [string range $line 26 50]
    set arrVariables(ring) [string range $line 52 55]
    set arrVariables(call_duration) [string range $line 57 66]
    set arrVariables(acc_code) [string range $line 66 76]
    set arrVariables(call_code) [string range $line 77 81]
    # Проверяем признак входящщего звонка
    if {$arrVariables(dial_number) == "<I>"} {
        set arrVariables(call_direct) "In"
        set arrVariables(dial_number) ""
    } elseif {[string range $arrVariables(dial_number) 0 3] == "EXT"} {
        set arrVariables(call_direct) "Ext"
        set arrVariables(dial_number) [string range $arrVariables(dial_number) 3 end]
    } else {
        set arrVariables(call_direct) "Out"
    }
    InsertData
}

proc Read {} {
    global fh
    if {[gets $fh line] >= 0} {
	ParceString $line
    }
}

proc PortDataRead {portName} {
    global out_dir fh
    set fh [open $portName RDONLY]
    fconfigure $fh -blocking 0 -buffering line -mode 9600,n,8,1 -translation crlf -eofchar {}
    fileevent $fh readable Read
    vwait forever
}

# Обработка ключей командной сроки
if {[llength $argv] >= 2} {
    if {[lindex $argv 0] == "-conf"} {
        source [lindex $argv 1]
    } else {
	puts "Не указан конфигурационный файл"
    }
    if {[lindex $argv 2] == "-port"} {
        set port_name [lindex $argv 3]
        PortDataRead $port_name
    }
    if {[lindex $argv 2] == "-file"} {
        set log_file_name [lindex $argv 3]
        set log [open $log_file_name "r"]
        # проверям наличие каталога и если его нет то создаём
        if {[file isdirectory $out_dir] == 0} {
            file mkdir $out_dir
        }
        # читаем файл построчно
        while {[gets $log line] >= 0} {
            ParceString $line
        }
        close $log
    }
} else {
    puts "\nФормат вызова:\n- для обработки файла\
    \n # -conf config.tcl
    \n # tclsh logger.tcl -conf config.tcl -file TDA20013082015_12052016.lg\
    \n- для чтения данных напрямую с com-порта АТС\
    \n # tclsh logger.tcl -conf config.tcl -port /dev/ttyUSB0\n"
    exit
}

